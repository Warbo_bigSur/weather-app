//
//  WeatherManager.swift
//  Clima
//
//  Created by warbo on 04/10/2021.
//  Copyright © 2021 App Brewery. All rights reserved.
//

import Foundation
import CoreLocation
protocol WeatherManagerDelegate {
    func didUpdateWeather( _ weatherManager: WeatherManager, weather: WeatherModel)
    func didFailWithError(error: Error)
    
}
struct WeatherManager {
    let weatherURL = "https://api.openweathermap.org/data/2.5/weather?appid=86a159ab61c89bf32f56c6a84298cc4c&units=metric"
    var delegate: WeatherManagerDelegate?
    
    func fetchWeather(cityName: String) {
        let urlString = ("\(weatherURL)&q=\(cityName)")
     
        performRequest(with: urlString)
    }
    func fetchWeather(latatude:CLLocationDegrees ,longtatude:CLLocationDegrees){
        let urlString = ("\(weatherURL)&lat=\(latatude)&lon=\(longtatude)")
        performRequest(with: urlString)
    }
    func performRequest(with urlString: String) {
        //1.Create a url
        if let url = URL(string: urlString){
            //2.Create a session
            let session = URLSession(configuration: .default)
            //3.Give the session task
            let task = session.dataTask(with: url) { data, reponse, error in
                if error != nil {
                    self.delegate?.didFailWithError(error: error!)
                    return
                }
                if let safeData = data {
                    if let weather = self.parseJSON(safeData){
                        delegate?.didUpdateWeather(self,weather: weather)
                    }
                }
            }
            //4. Start the task
            task.resume()
        }
    }
    func parseJSON(_ weatherData: Data)-> WeatherModel?{
        let decoder = JSONDecoder()
        do{
            let decodedData = try decoder.decode(WeatherData.self , from: weatherData )
            let id = decodedData.weather[0].id
            let name = decodedData.name
            let temp = decodedData.main.temp
            let weather = WeatherModel(conditionId: id, cityName: name, temperature: temp)
            print(weather.temperatureString)
            return weather
        }catch{
            delegate?.didFailWithError(error: error)
        return nil
        }
        
    }
    
   
}
